import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

import com.sun.xml.internal.ws.api.pipe.NextAction;

public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);


		Hashtable<String, String[]> productos = new Hashtable<String, String[]>();

		//Productos predefinidos
		productos.put("Pan", new String[] {"0.5", "7"});
		productos.put("Agua", new String[] {"1.5", "7"});
		productos.put("Refresco", new String[] {"2.5", "7"});
		productos.put("Fruta", new String[] {"3.5", "7"});
		productos.put("Verdura",new String[] {"4.5", "7"});
		productos.put("Carne", new String[] {"5.5", "7"});
		productos.put("Consola", new String[] {"400.99", "7"});
		productos.put("Ordenador", new String[] {"899.5", "7"});
		productos.put("Movil", new String[] {"999.5", "7"});

		int iniciar = 0;
		//Panel
		while (iniciar != -1) {
			System.out.println("Que quieres hacer? (1 - editar productos, 2 - comprar productos, -1 para salir)");
			iniciar = teclado.nextInt();

			if(iniciar == 1) {
				//Trabajador
				System.out.println("Que desea hacer(1- a�adi producto, 2 - Visualizar precio de un producto, 3 -listar todos los productos, -1 - salir)");
				int opcion = teclado.nextInt();
				while (opcion != -1) {

					switch (opcion) {
					case 1:
						addProducts(productos);
						break;
					case 2:
						show(productos);
						break;
					case 3:
						list(productos);
						break;
					}
					System.out.println("Que desea hacer(1- a�adi producto, 2 - Visualizar precio de un producto, 3 -listar todos los productos, -1 - salir)");
					opcion = teclado.nextInt();

				}
			}else {
				//Cliente
				ArrayList<Double> carrito = new ArrayList<Double>();
				comprarProductos(carrito, productos);
				if(productos.size() != 0) {
					double precioTotalIva = IVAProductos(carrito);
					System.out.println("Se ha aplicado un 21%de IVA: " + precioTotalIva + "�");
					System.out.println("El total bruto es: " + brutoProductos(carrito) + "�");
					System.out.println("La cantidad de productos son " + carrito.size());
					System.out.println("Introduzca el dinero a pagar. Total : " + precioTotalIva);
					System.out.println("El cambio es un total de " + pagar(precioTotalIva) + "�");
				}
			}
		}
	}


	//A�adir productos
	public static void addProducts(Hashtable<String, String[]> productos) {
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduce el nombre del producto");
		String articulo = teclado.next();

		System.out.println("Introduce el precio del producto");
		String precio = String.valueOf(teclado.nextDouble());

		System.out.println("Introduce el cantidad del producto");
		String cantidad = String.valueOf(teclado.nextInt());

		productos.put(articulo, new String[] {precio, cantidad});
	}

	//Mostrar el precio y cantidad de un producto
	public static void show(Hashtable<String, String[]> productos) {
		Scanner teclado = new Scanner(System.in);

		System.out.println("Indica que producto quieres ver el precio");
		String prod = teclado.next();
		System.out.println(prod + ":");
		System.out.println("Precio: " + productos.get(prod)[0] + "�");
		System.out.println("Cantidad: " + productos.get(prod)[1] + "/uds");
	}
	
	//Lista todos los productos disponibles
	public static void list(Hashtable<String, String[]> productos) {
		Enumeration<String> key = productos.keys();
		Enumeration<String[]> precioCantidad = productos.elements();


		while(key.hasMoreElements()) {
			String array[] = precioCantidad.nextElement();
			System.out.println(key.nextElement() + " -> " + array[0] + "� - Cantidad en stock: " + array[1] + "/uds");
		}
	}

	//Comprar productos
	public static void comprarProductos(ArrayList<Double> carrito, Hashtable<String, String[]> productos) {
		Scanner teclado = new Scanner(System.in); 
		String productoComprar;
		int count = 0;

		System.out.println("Introduce el nombre del producto que deseas comprar  (-1 para salir)");
		productoComprar = teclado.next();
		
		while(!productoComprar.equals("-1")) {
			//Muestra el producto a comprar y su precio
			System.out.println(productoComprar + " cuesta " + productos.get(productoComprar)[0] + "�");
			
			System.out.println("Cuantos quieres comprar de este producto?");
			int cantidadComprar = teclado.nextInt();
			//guardo la cantidad que hay en stock de ese producto
			int stock = Integer.parseInt(productos.get(productoComprar)[1]);

				//Si hay stock
			if(stock-cantidadComprar >= 0) {
				//Resto la cantidad que hay por la que quiero comprar. Al remplazarlo tengo que volver a coger el precio del producto que estoy modificando
				productos.replace(productoComprar, new String[] {productos.get(productoComprar)[0],  String.valueOf(stock-cantidadComprar) });

				//A�ado todos los productos individualmente en el ArrayList
				for (int i = 0; i < cantidadComprar; i++) {
					carrito.add(Double.parseDouble(productos.get(productoComprar)[0]));
				}
				
			}else {
				System.out.println("No hay suficiente stock");
			}
			System.out.println("A�ade el nombre del siguiente producto que deseas comprar (-1 para salir)");
			productoComprar = teclado.next();

		}

	}

	public static double IVAProductos(ArrayList<Double> productos) {

		double totalIVA = 0;
		for (Double o:productos) {
			totalIVA += o*1.21;
		}
		return totalIVA;

	}

	public static double brutoProductos(ArrayList<Double> productos) {

		double totalBruto = 0;
		for (Double o:productos) {
			totalBruto += o;
		}
		return totalBruto;

	}

	public static double pagar (double necesario) {
		Scanner teclado = new Scanner(System.in);
		double pago = teclado.nextDouble();

		while (pago < necesario) {
			System.out.println("Dinero insuficiente");
			pago = teclado.nextDouble();
		}
		return necesario-pago;
	}
}
