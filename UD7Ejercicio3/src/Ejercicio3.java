import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);

		Hashtable<String, Double> productos = new Hashtable<String, Double>();
		productos.put("Pan", 0.5);
		productos.put("Agua", 1.5);
		productos.put("Refresco", 2.5);
		productos.put("Fruta", 3.5);
		productos.put("Verdura", 4.5);
		productos.put("Carne", 5.5);
		productos.put("Consola", 400.99);
		productos.put("Ordenador", 899.5);
		productos.put("Movil", 999.5);

		System.out.println("Que desea hacer(1- a�adi producto, 2 - Visualizar precio de un producto, 3 -listar todos los productos)");
		int opcion = teclado.nextInt();
		while (opcion != -1) {

			switch (opcion) {
			case 1:
				addProducts(productos);
				break;
			case 2:
				show(productos);
				break;
			case 3:
				list(productos);
				break;
			}
			System.out.println("Que desea hacer(1- a�adi producto, 2 - Visualizar precio de un producto, 3 -listar todos los productos)");
			opcion = teclado.nextInt();

		}
	}

	public static void addProducts(Hashtable<String, Double> productos) {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce el nombre del producto");
		String articulo = teclado.next();
		
		System.out.println("Introduce el precio del producto");
		double precio = teclado.nextDouble();

		productos.put(articulo, precio);
	}

	public static void show(Hashtable<String, Double> productos) {
		Scanner teclado = new Scanner(System.in);

		System.out.println("Indica que producto quieres ver el precio");
		System.out.println(productos.get(teclado.next()));
	}

	public static void list(Hashtable<String, Double> productos) {
		Enumeration<String> key = productos.keys();
		Enumeration<Double> precio = productos.elements();


		while(key.hasMoreElements()) {
			System.out.println(key.nextElement() + " -> " + precio.nextElement() + "�");
		}
	}

}
