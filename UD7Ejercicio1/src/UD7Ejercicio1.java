import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

//Crea una aplicación que calcule la nota media de los
//alumnos pertenecientes al curso de programación. Una vez
//calculada la nota media se guardara esta información en un
//diccionario de datos que almacene la nota media de cada
//alumno. Todos estos datos se han de proporcionar por
//pantalla.


public class UD7Ejercicio1 {

	public static void main(String[] args) {
		
	Hashtable<String, Integer> notas = new Hashtable<String, Integer>();
		
	añadirNotas(notas);
	mostrarNotas(notas);
	}
	
	public static void añadirNotas(Hashtable<String, Integer> notas ) {
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Cuantos alumnos hay");
		int alumnos = teclado.nextInt();
		System.out.println("Cuantas notas tienen los alumnos");
		int cantidadNotas = teclado.nextInt();

		for (int i = 0; i < alumnos; i++) {
			System.out.println("Introduce el nombre del alumno");
			teclado.nextLine();
			String nombre = teclado.nextLine();
			int sumaNotas = 0;
			for (int j = 0; j < cantidadNotas; j++) {
				System.out.println("introduce la nota de la asignaura " + (1+j));
				sumaNotas+= teclado.nextInt();
				
			}
			notas.put(nombre, (sumaNotas/cantidadNotas));
		}
		
	}
	public static void mostrarNotas(Hashtable<String, Integer> notas ) {
		Enumeration<String> keyNotas = notas.keys();
		Enumeration<Integer> elemNotas = notas.elements();
		while(keyNotas.hasMoreElements()) {
			System.out.println("Alumno: " + keyNotas.nextElement());
			System.out.print(" Nota: " + elemNotas.nextElement());
		}
		
	}

}
