import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio2App {

	public static void main(String[] args) {

		ArrayList<Double> productos = new ArrayList<Double>();
		añadirProductos(productos);
		if(productos.size() != 0) {
			double precioTotalIva = IVAProductos(productos);
			System.out.println("Se ha aplicado un 21%de IVA: " + precioTotalIva + "€");
			System.out.println("El total bruto es: " + brutoProductos(productos) + "€");
			System.out.println("La cantidad de productos son " + productos.size());
			System.out.println("Introduzca el dinero a pagar. Total : " + precioTotalIva);
			System.out.println("El cambio es un total de " + pagar(precioTotalIva) + "€");
		}


	}

	public static void añadirProductos(ArrayList<Double> productos) {
		Scanner teclado = new Scanner(System.in); 
		double precio = 0;
		int count = 0;
		
		System.out.println("Introduce el precio del porducto " + (++count) + "(si no quieres añadir mas productos escribe -1) " );
		precio = teclado.nextDouble();
		while(precio != -1) {
			productos.add(precio);
			System.out.println("Introduce el precio del porducto " + (++count));
			precio = teclado.nextDouble();

		}


	}
	public static double IVAProductos(ArrayList<Double> productos) {

		double totalIVA = 0;
		for (Double o:productos) {
			totalIVA += o*1.21;
		}
		return totalIVA;

	}

	public static double brutoProductos(ArrayList<Double> productos) {

		double totalBruto = 0;
		for (Double o:productos) {
			totalBruto += o;
		}
		return totalBruto;

	}

	public static double pagar (double necesario) {
		Scanner teclado = new Scanner(System.in);
		double pago = teclado.nextDouble();
		
		while (pago < necesario) {
			System.out.println("Dinero insuficiente");
			 pago = teclado.nextDouble();
		}
		return necesario-pago;
	}


}
